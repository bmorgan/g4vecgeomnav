cmake_minimum_required(VERSION 3.12...3.20)

project(G4VecGeomNav)

if(NOT CMAKE_BUILD_TYPE)
    message(WARNING "CMAKE_BUILD_TYPE not set : will use RelWithDebInfo")
    set(CMAKE_BUILD_TYPE RelWithDebInfo)
endif()

# option to build with ROOT support, which is mainly
# use as an additional navigator to compare too in tests
# or for use in VMC simulation
option(WITH_ROOT "WITH ROOT SUPPORT" OFF)

# Option to enable / disable test.
#   To enable this to be fetch for building applications,
option(BUILD_NAV_TEST "Build the test application(s)" OFF)

find_package(Geant4 REQUIRED NO_MODULE)
find_package(VecGeom 1.1.10 REQUIRED NO_MODULE)
if (WITH_ROOT)
  find_package(ROOT REQUIRED)
  find_package(TGeo2VecGeom REQUIRED)
  add_compile_definitions(WITH_ROOT)
endif()

#-------------------------------------------------q
# Locate sources for this project
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cxx)
set(sources src/G4VecGeomConverter.cxx
            src/TG4VecGeomNavigator.cxx
            src/TG4VecGeomIncNavigator.cxx
            # src/TG4VecGeomNavMgr.cxx
            src/TG4VecGeomVoxelNavigation.cxx)
if (WITH_ROOT)
set(sources ${sources}
        src/TG4RootVecGeomIncNavigator.cxx
        src/TG4RootDetectorConstruction.cxx
        src/TG4RootSolid.cxx
        src/TG4RootNavigator.cxx
        src/TG4VecGeomDetectorConstruction.cxx)
endif()

# define the main target : The G4VecGeom library
add_library(g4vecgeomnav SHARED ${sources})
add_library(G4VecGeomNav::g4vecgeomnav ALIAS g4vecgeomnav)

target_include_directories(
        g4vecgeomnav
        PUBLIC
        ${VECGEOM_INCLUDE_DIR}
        ${VECGEOM_EXTERNAL_INCLUDES}
        ${Geant4_INCLUDE_DIR}
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

# we depend on VecGeom / ROOT / G4
target_link_libraries(g4vecgeomnav
        PUBLIC
        VecGeom::vecgeom
        ${Geant4_LIBRARIES}
        )
if(WITH_ROOT)
target_link_libraries(g4vecgeomnav
        PUBLIC
        ROOT::Geom
        ROOT::Core
        TGeo2VecGeom::TGeo2VecGeom
        )
endif()

# build test applications
if(BUILD_NAV_TEST)
       add_subdirectory(tests/FullCMS)
endif()

# installation specific
include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/G4VecGeomNav)

install(TARGETS g4vecgeomnav
        EXPORT g4vecgeomnav-targets
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        )

#This is required so that the exported target has the name G4VecGeomNav and not g4vecgeomnav
set_target_properties(g4vecgeomnav PROPERTIES EXPORT_NAME G4VecGeomNav)

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

#Export the targets to a script
install(EXPORT g4vecgeomnav-targets
        FILE
        G4VecGeomNavTargets.cmake
        NAMESPACE
        G4VecGeomNav::
        DESTINATION
        ${INSTALL_CONFIGDIR}
        )

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
#write_basic_package_version_file(
#        ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfigVersion.cmake
#        VERSION ${PROJECT_VERSION}
#        COMPATIBILITY AnyNewerVersion
#)

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/G4VecGeomNavConfig.cmake.in
        ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfig.cmake
        INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
        )

#Install the config, configversion and custom find modules
install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfig.cmake
        # ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavConfigVersion.cmake
        DESTINATION ${INSTALL_CONFIGDIR}
        )

# Attemps to identify include files ... the next line seems to break cmake config/build in the CI
# set(G4VecGeomNav_INCLUDE_DIRS ${CMAKE_INSTALL_PREFIX}/include) # ${CMAKE_INSTALL_INCLUDEDIR}) # ${PREFIX}/include)
message(STATUS " - G4VecGeomNav_INCLUDE_DIRS = " ${G4VecGeomNav_INCLUDE_DIRS})
# Label the library -- to review ??
set(G4VecGeomNav_LIBRARIES ${G4VECGEOMNAV_LIBRARIES} g4vecgeomnav)

##############################################
## Exporting from the build tree

export(EXPORT g4vecgeomnav-targets FILE ${CMAKE_CURRENT_BINARY_DIR}/G4VecGeomNavTargets.cmake NAMESPACE G4VecGeomNav::)

#Register package in user's package registry
export(PACKAGE G4VecGeomNav)
