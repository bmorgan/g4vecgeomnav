#include "TG4VecGeomDetectorConstruction.h"
#ifdef WITH_ROOT
#include <TGeo2VecGeom/RootGeoManager.h>
#endif

void TG4VecGeomDetectorConstruction::CheckVolumeConsistency(PVolume_t const *vg, G4VPhysicalVolume const *g4)
{
  if (g4 == nullptr) {
    // std::cerr << "COMPARING A NULLPTR VOLUME\n";
    assert(vg == nullptr);
    return;
  }
  if (vg == nullptr) {
    assert(g4 == nullptr);
    return;
  }

  // check placement
  auto vgtrans = vg->GetTransformation()->Translation();
  auto vgrot   = vg->GetTransformation()->Rotation();

  auto g4rot   = g4->GetObjectRotation();
  auto g4trans = g4->GetObjectTranslation();

  // check translation
  assert(g4trans[0] == vgtrans[0]);
  assert(g4trans[1] == vgtrans[1]);
  assert(g4trans[2] == vgtrans[2]);

  // check rotation
  assert((*g4rot)[0][0] == vgrot[0]);
  assert((*g4rot)[0][1] == vgrot[1]);
  assert((*g4rot)[0][2] == vgrot[2]);
}

void TG4VecGeomDetectorConstruction::ConnectVecGeomG4()
{
  // resize the lookup
  fFastG4VGLookup.fPV_VGtoG4_Vector.resize(vecgeom::GeoManager::Instance().GetPlacedVolumesCount());
  fFastG4VGLookup.fPV_G4toVG_Vector.resize(vecgeom::GeoManager::Instance().GetPlacedVolumesCount());

  for (auto iter : fRootG4DetConstruction->fG4PVolumeMap) {
    std::cout << "MAP " << iter.first << " " << iter.second << "\n";

    // make the connection to VecGeom
    auto &rootmgr  = tgeo2vecgeom::RootGeoManager::Instance();
    auto vecgeompl = rootmgr.Lookup(iter.first);
    if (!vecgeompl) {
      std::cerr << "VecGeom instance is null\n";
    }

    // fill the lookup structures
    auto g4lv = iter.second->GetLogicalVolume();
    auto vglv = vecgeompl->GetLogicalVolume();
    fLV_G4toVG_Map.insert(std::pair<const G4LogicalVolume *, const LVolume_t *>(g4lv, vglv));
    fLV_VGtoG4_Map.insert(std::pair<const LVolume_t *, const G4LogicalVolume *>(vglv, g4lv));
    fPV_G4toVG_Map.insert(std::pair<const G4VPhysicalVolume *, const PVolume_t *>(iter.second, vecgeompl));
    fPV_VGtoG4_Map.insert(std::pair<const PVolume_t *, const G4VPhysicalVolume *>(vecgeompl, iter.second));
    // fFastG4VGLookup.fPV_VGtoG4_Vector[vecgeompl->id()] = iter.second;
    fFastG4VGLookup.Insert(vecgeompl, iter.second);

    std::cout << "MAPV " << vecgeompl << " with ID " << vecgeompl->id() << "\n";
    std::cout << "G4ID " << (iter.second)->GetInstanceID() << "\n";
    // fFastG4VGLookup.fPV_G4toVG_Vector[((MYG4VP*)iter.second)->getID()] = vecgeompl;

    // do some cross checks -- to be put into sep function?
    // std::cout << vecgeompl->GetLogicalVolume()->GetNDaughters() << " "
    //          << iter.second->GetLogicalVolume()->GetNoDaughters() << "\n";

    // check placement
    auto vgtrans = vecgeompl->GetTransformation()->Translation();
    auto vgrot   = vecgeompl->GetTransformation()->Rotation();

    auto g4rot   = iter.second->GetObjectRotation();
    auto g4trans = iter.second->GetObjectTranslation();

    // check translation
    assert(g4trans[0] == vgtrans[0]);
    assert(g4trans[1] == vgtrans[1]);
    assert(g4trans[2] == vgtrans[2]);

    std::cout << "Tx " << g4trans[0] << " " << vgtrans[0] << "\n";
    std::cout << "Ty " << g4trans[1] << " " << vgtrans[1] << "\n";
    std::cout << "Tz " << g4trans[2] << " " << vgtrans[2] << "\n";

    // check rotation
    std::cout << "R0 " << (*g4rot)[0][0] << " " << vgrot[0] << "\n";
    std::cout << "R1 " << (*g4rot)[0][1] << " " << vgrot[1] << "\n";
    std::cout << "R2 " << (*g4rot)[0][2] << " " << vgrot[2] << "\n";

    // check bounding boxes
  }
}

G4VPhysicalVolume *TG4VecGeomDetectorConstruction::Construct()
{
  if (fIsConstructed) return fTopPV;

  std::cerr << "constructing \n";
  // call construct on TGeo to G4 version
  auto top = fRootG4DetConstruction->Construct();
  //
  std::cout << "Now loading VecGeom geometry\n";
  tgeo2vecgeom::RootGeoManager::Instance().EnableG4Units();
  tgeo2vecgeom::RootGeoManager::Instance().LoadRootGeometry();
  std::cout << "Finished loading geometry\n";

  // do the VecGeom - G4 mapping
  ConnectVecGeomG4();
  fIsConstructed = true;
  fTopPV         = top;
  return top;
}

void TG4VecGeomDetectorConstruction::ConstructSDandField()
{
  std::cerr << "HUHUHU\n";
}

TG4VecGeomDetectorConstruction::~TG4VecGeomDetectorConstruction() {}
