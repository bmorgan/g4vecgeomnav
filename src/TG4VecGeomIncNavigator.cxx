/*
 * TG4VecGeomIncNavigator.cxx
 *
 *  Created on: Mar 1, 2018
 *      Author: swenzel
 */

#include "TG4VecGeomIncNavigator.h"

#include <VecGeom/management/GeoManager.h>
#include <VecGeom/navigation/NewSimpleNavigator.h>
#include <VecGeom/navigation/VSafetyEstimator.h>
#include "FastG4VecGeomLookup.h"
// #include "TG4VecGeomDetectorConstruction.h"
#include <VecGeom/base/Vector3D.h>
#include <VecGeom/base/Transformation3D.h>
#include <cmath>

using V3 = vecgeom::Vector3D<double>;

TG4VecGeomIncNavigator::TG4VecGeomIncNavigator(FastG4VecGeomLookup const &dc) : G4Navigator(), fG4VGLookup(&dc)
{
  std::cout << "INIT TG4VecGeomIncNavigator sizeof(G4Navigator) " << sizeof(G4Navigator) << " sizeof(VecGeomNavi) "
            << sizeof(TG4VecGeomIncNavigator) << "\n";
  std::cout << "  Configuration : Replacing ";
#ifdef REPLACE_COMPUTESTEP
  std::cout << " ComputeStep";
#endif
#ifdef REPLACE_COMPUTESAFETY
  std::cout << " ComputeSafety";
#endif
#if defined(REPLACE_COMPUTESTEP) || defined(REPLACE_COMPUTESAFETY)
  std::cout << " NOTHING -- unexpected. \n";
#else
  std::cout << " \n";
#endif
}

G4VPhysicalVolume *TG4VecGeomIncNavigator::UpdateInternalHistory()
{
  // updates fHistory to be consistent with G4 logical geometry
  // very crude thing ... needs to be optimize
  fHistory.Clear();
  assert(fHistory.GetDepth() == 0);
  assert(fHistory.GetTopVolume() == nullptr);
  for (int l = 0; l < fCurState->GetCurrentLevel(); ++l) {
    int replica    = 0;
    bool isreplica = false;
    auto g4pv      = fG4VGLookup->VGToG4(fCurState->At(l)->id(), replica, isreplica);
    // std::cerr << "GOT REPLICA " << replica << "\n";

    // auto g4pv = fG4VGLookup->fPV_VGtoG4_Vector[fCurState->At(l)->id()];
    if (l == 0) {
      assert(isreplica == false);
      fHistory.SetFirstEntry(g4pv);
    } else {
      if (isreplica) {
        // unfortunately, we need to set the transform of the replica here
        // inspired from G4ReplicaNavigation::ComputeTransformation()
        const auto &tr = fCurState->At(l)->GetTransformation();
        g4pv->SetTranslation(G4ThreeVector(tr->Translation(0), tr->Translation(1), tr->Translation(2)));
        fHistory.NewLevel(g4pv, kReplica, replica);
      } else {
        fHistory.NewLevel(g4pv);
      }
    }
  }
  //  TG4VecGeomDetectorConstruction::CheckVolumeConsistency(fCurState->Top(),
  //  fHistory.GetTopVolume());
  //  if ( fCurState->Top() != nullptr ){
  //    assert(fG4VGLookup->fPV_VGtoG4_Vector[fCurState->Top()->id()] ==
  //    fHistory.GetTopVolume());
  //  }
  //  else {
  //	assert(nullptr == fHistory.GetTopVolume());
  //  }
  // assert(fHistory.GetDepth() == fCurState->GetCurrentLevel() - 1);
  return fHistory.GetTopVolume();
}

void TG4VecGeomIncNavigator::UpdateVecGeomState()
{
  fCurState->Clear();
  for (std::size_t l = 0; l <= fHistory.GetDepth(); ++l) {
    // TODO: generalize for case of lookup
    // TODO: how to get the copy number?
    // const auto pv = fG4VGLookup->fPV_G4toVG_Vector[((MYG4VP*)fHistory.GetVolume(l))->getID()];
    // const int replica = 0;

    // const int replica = fHistory.GetReplicaNo(l) - 1; // replicas start at 1?
    const auto g4pv   = fHistory.GetVolume(l);
    const int g4id    = g4pv->GetInstanceID();
    // std::cerr << " level " << l << " g4pvol " << g4pv << " " << g4pv->GetName() << " " <<  g4id << " replica " <<
    // replica << " isreplicated " << g4pv->IsReplicated() << " \n";
    const int replica2 = 0; //(replica == -1) ? 0 : replica;
    const auto pv      = fG4VGLookup->G4ToVG(g4id, replica2);
    fCurState->Push(pv);
  }
  // TG4VecGeomDetectorConstruction::CheckVolumeConsistency(fCurState->Top(), fHistory.GetTopVolume());
}

#ifdef REPLACE_COMPUTESAFETY
G4double TG4VecGeomIncNavigator::ComputeSafety(const G4ThreeVector &globalpoint, const G4double /*pProposedMaxLength*/,
                                               const G4bool /*keepState*/)
{
  // std::cerr << "CS called\n";
  // there is nothing to do on the boundary (but we cross check with point for some reason)
  if (fEnteredDaughter || fExitedMother) {
    // std::cerr << "exiting first condition\n";
    // return 0.;
    // }

    // if we didn't move also return 0. (as done by G4)
    // this seems to be quite an important optimization
    const G4double distEndpointSq = (globalpoint - fStepEndPoint).mag2();
    const G4bool stayedOnEndpoint = distEndpointSq < kCarTolerance * kCarTolerance;
    if (stayedOnEndpoint) {
      // std::cerr << "stayed on point " << globalpoint << " " << fStepEndPoint << "\n";
      return 0.;
    }
  }

  // std::cerr << "calc safety\n";

  // we need to get the VecGeom state from the current G4 state
  // if (!fCurState) {
  //  fCurState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
  //  fTestState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
  //}

  // UpdateVecGeomState();
  // compute local point in G4 world
  // ComputeSafetyForLocalPoint(Vector3D<Precision> const & /*localpoint*/, VPlacedVolume);
  const auto g4pvvol = fHistory.GetTopVolume();
  assert(g4pvvol->IsReplicated() == false); // for the moment no replication

  // get VecGeom equivalent for current mother
  const int replica              = 0; // fHistory.GetReplicaNo(l);
  const auto pv                  = fG4VGLookup->G4ToVG(((MYG4VP *)g4pvvol)->getID(), replica);
  const auto localpoint          = fHistory.GetTopTransform().TransformPoint(globalpoint);
  const auto bestsafetyestimator = pv->GetLogicalVolume()->GetSafetyEstimator();
  const auto safety =
      bestsafetyestimator->ComputeSafetyForLocalPoint(V3(localpoint[0], localpoint[1], localpoint[2]), pv);

  // Check what G4 is doing as well!!
  if (safety < 0.) {
    std::cerr << "Negative safety for " << globalpoint[0] << " " << globalpoint[1] << " " << globalpoint[2] << " "
              << bestsafetyestimator->GetName() << "\n";
    // fCurState->Print();
    // fTestState->Clear();
    if (!fCurState) {
      fCurState  = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
      fTestState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
    }

    const auto &mgr = vecgeom::GeoManager::Instance();
    fTestState->Clear();
    vecgeom::GlobalLocator::LocateGlobalPoint(mgr.GetWorld(), V3(globalpoint[0], globalpoint[1], globalpoint[2]),
                                              *fTestState, true);
    fTestState->Print();
  }
  // std::cerr << "RETURNING " << std::max(0., safety) << "\n";
  return std::max(0., safety);
}
#endif

#ifdef REPLACE_COMPUTESTEP
G4double TG4VecGeomIncNavigator::ComputeStep(const G4ThreeVector &pGlobalPoint, const G4ThreeVector &pDirection,
                                             const G4double pCurrentProposedStepLength, G4double &pNewSafety)
{
  static int iStep = 0;
  iStep++;
  pNewSafety = 0; // make sure this variable is initialized

  // calculate current vecgeom state
  // we need to get the VecGeom state from the current G4 state
  if (!fCurState) {
    fCurState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
  }
  UpdateVecGeomState();
  fCurState->Print();

  const auto curpv            = fCurState->Top();
  const auto curlevel         = fCurState->GetCurrentLevel();
  const auto bestnav          = curpv->GetLogicalVolume()->GetNavigator();
  const bool indicateEntering = true;

  // false == we never calculate the safety here
  V3 gp(pGlobalPoint[0], pGlobalPoint[1], pGlobalPoint[2]);
  V3 gd(pDirection[0], pDirection[1], pDirection[2]);
  // determine entering or leaving situation (or none)
  vecgeom::Transformation3D m;
  fCurState->TopMatrix(m);
  auto localpoint = m.Transform(gp);
  auto localdir   = m.TransformDirection(gd);

  auto step = bestnav->ComputeStepAndSafety(gp, gd, pCurrentProposedStepLength, *fCurState, false, pNewSafety,
                                            indicateEntering);

  // call the compute safety method from the navigator to calc the safety
  pNewSafety = ComputeSafety(pGlobalPoint, pCurrentProposedStepLength, true);

  std::cerr << "step " << step << " " << pNewSafety << " " << pCurrentProposedStepLength << "\n";

  const auto state_changed = fCurState->GetCurrentLevel() > curlevel;
  double rawStep           = step;
  const double minstep     = 1.E-6;
  if (step <= 0) step = minstep;

  bool entering, exiting;
  if (step < pCurrentProposedStepLength) // geometry step
  {
    entering               = state_changed; // in case of a geometry step it is one of the 2
    exiting                = !entering;
    fStepEndPoint          = pGlobalPoint + step * pDirection;
    auto localendpoint     = localpoint + step * localdir;
    fLastStepEndPointLocal = G4ThreeVector(localendpoint[0], localendpoint[1], localendpoint[2]);
    fWasLimitedByGeometry  = true;
  } else {
    entering              = false;
    exiting               = false;
    step                  = kInfinity;
    fWasLimitedByGeometry = false;
  }
  InformLastStep(rawStep, entering, exiting); // needs raw step -- to detect zero steps
  return step;
}
#endif

void TG4VecGeomIncNavigator::CheckStates() const
{
  G4ThreeVector g4point(1, 1, 0.5);
  auto p1 = GetLocalVGPoint(g4point);

  vecgeom::Vector3D<double> vgpoint(1, 1, 0.5);
  vecgeom::Transformation3D m;
  fCurState->TopMatrix(m);
  auto p2 = m.Transform(vgpoint);

  if (std::abs(p1[0] - p2[0]) > 1E-6) {
    std::cerr << "ERROR\n";
  }
  if (std::abs(p1[1] - p2[1]) > 1E-6) {
    std::cerr << "ERROR\n";
  }
  if (std::abs(p1[2] - p2[2]) > 1E-6) {
    std::cerr << "ERROR\n";
  }

  // access the G4AffineTransformations and calculate difference
  std::vector<G4AffineTransform> transforms;
  G4NavigationHistory hist(fHistory);
  while (hist.GetDepth() > 0) {
    transforms.emplace_back(hist.GetTopTransform());
    hist.BackLevel();
  }

//  if (transforms.size() >= 2) {
//    auto diff = transforms[0] * transforms[1].Inverse();
//  }
}
