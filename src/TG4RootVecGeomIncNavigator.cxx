#include "TG4RootVecGeomIncNavigator.h"

#include <VecGeom/management/GeoManager.h>
#include <TGeo2VecGeom/RootGeoManager.h>
#include <VecGeom/navigation/NewSimpleNavigator.h>
#include <VecGeom/navigation/VSafetyEstimator.h>
#include "FastG4VecGeomLookup.h"
#include "TG4VecGeomDetectorConstruction.h"
#include <VecGeom/base/Vector3D.h>
#include <VecGeom/base/Transformation3D.h>
#include <VecGeom/volumes/PlacedVolume.h>
#include "G4SystemOfUnits.hh"

#include "TGeoManager.h"
#include <cmath>
#undef NDEBUG
#include <cassert>

using V3 = vecgeom::Vector3D<double>;

TG4RootVecGeomIncNavigator::TG4RootVecGeomIncNavigator(TG4RootDetectorConstruction const *dc)
    : TG4RootNavigator(const_cast<TG4RootDetectorConstruction *>(dc))
{
  // we need to create the conversion lookups with G4
  // a) iterate over all TGeo nodes --> get corresponding VecGeom placedvolume and G4 placedvolume
  // and fill the structure

  // THIS SHOULD HAPPEN OUTSIDE IDEALLY SINCE SHOULD ONLY BE DONE ONCE

  // the manager knowing about VecGeom <-> ROOT
  auto &rootmgr = tgeo2vecgeom::RootGeoManager::Instance();

  // convert TGeo to VecGeom
  if (rootmgr.world() == nullptr) {
    assert(gGeoManager);
    rootmgr.LoadRootGeometry();
  }

  auto &vecgeommgr = vecgeom::GeoManager::Instance();
  // iterate over all TGeo nodes
  // how do we do iteration?? Can iterate over VecGeom equivalents ??

  std::vector<vecgeom::VPlacedVolume *> vecgeompvolumes;
  vecgeommgr.getAllPlacedVolumes(vecgeompvolumes);

  fG4VGLookup.InitSize(vecgeom::VPlacedVolume::GetIdCount());
  int counter = 0;
  for (auto pv : vecgeompvolumes) {
    // get the corresponding g4pl via ROOT-G4 tables
    auto rootnode = rootmgr.tgeonode(pv);
    auto g4pl     = fDetConstruction->GetG4VPhysicalVolume(rootnode);

    if (!rootnode || !pv || !g4pl) {
      std::cerr << " NULLPTR PROBLEM \n";
    }
    // std::cerr << "VG " << pv << " " << rootnode << " " << g4pl << "\n";

    fG4VGLookup.Insert(pv, g4pl);
    counter++;
  }

  std::cout << "Inserted " << counter << " volumes into VecGeom-G4 map\n";

  // we also need to assign navigator structures
}

#ifdef REPLACE_COMPUTESAFETY
G4double TG4RootVecGeomIncNavigator::ComputeSafety(const G4ThreeVector &globalpoint, const G4double pProposedMaxLength,
                                                   const G4bool keepState)
{
  // std::cerr << " CS called \n";
  // std::cerr << gGeoManager->GetCurrentNavigator()->GetPath() << "\n";
  // there is nothing to do on the boundary (but we cross check with point for some reason)
  const bool g4boundary   = fEnteredDaughter || fExitedMother;
  const bool tgeoboundary = fStepEntering || fStepExiting;

  if (g4boundary || tgeoboundary) {
    // std::cerr << "exiting first condition\n";
    // return 0.;
    // }

    // if we didn't move also return 0. (as done by G4)
    // this seems to be quite an important optimization
    const G4double distEndpointSq = (globalpoint - fStepEndPoint).mag2();
    const G4bool stayedOnEndpoint = distEndpointSq < kCarTolerance * kCarTolerance;
    if (stayedOnEndpoint) {
      // std::cerr << "stayed on point " << globalpoint << " " << fStepEndPoint << "\n";
      std::cerr << " returning 0.\n";
      return 0.;
    }
  }

  // std::cerr << "calc safety\n";

  // we need to get the VecGeom state from the current G4 state
  // if (!fCurState) {
  //  fCurState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
  //  fTestState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
  //}

  // UpdateVecGeomState();
  // compute local point in G4 world
  // ComputeSafetyForLocalPoint(Vector3D<Precision> const & /*localpoint*/, VPlacedVolume);
  const auto g4pvvol = fHistory.GetTopVolume();
  assert(g4pvvol->IsReplicated() == false); // for the moment no replication

  // get VecGeom equivalent for current mother
  const int replica              = 0; // fHistory.GetReplicaNo(l);
  const auto pv                  = fG4VGLookup.G4ToVG(g4pvvol->GetInstanceID(), replica);
  const auto localpoint          = fHistory.GetTopTransform().TransformPoint(globalpoint);
  const auto bestsafetyestimator = pv->GetLogicalVolume()->GetSafetyEstimator();

  const auto gCm = 1. / cm;

  assert(bestsafetyestimator);
  const auto safety = bestsafetyestimator->ComputeSafetyForLocalPoint(
      V3(localpoint[0] * gCm, localpoint[1] * gCm, localpoint[2] * gCm), pv);

  // std::cerr << " calc safety in " << g4pvvol->GetName() << " " << pv->GetName() << " : " << safety << " \n";

  // Check what G4 is doing as well!!
  if (safety < 0.) {
    // std::cerr << "Negative safety for " << globalpoint[0] << " " << globalpoint[1] << " " << globalpoint[2] << " "
    //         << bestsafetyestimator->GetName() << "\n";
    // fCurState->Print();
    // fTestState->Clear();
    //    if (!fCurState) {
    //      fCurState  = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
    //      fTestState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
    //    }
    //
    //    const auto &mgr = vecgeom::GeoManager::Instance();
    //    fTestState->Clear();
    //    vecgeom::GlobalLocator::LocateGlobalPoint(mgr.GetWorld(), V3(globalpoint[0], globalpoint[1], globalpoint[2]),
    //                                              *fTestState, true);
    // fTestState->Print();
  }
  // update ROOT state : fLastSafetyPoint; fLastSafety
  fSafetyOrig = globalpoint;
  fLastSafety = cm * std::max(0., safety);

  // std::cerr << "RETURNING " << std::max(0., safety) << "\n";
  return cm * std::max(0., safety);
}
#endif
