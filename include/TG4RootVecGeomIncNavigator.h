#ifndef G4VECGEOM_INCLUDE_TG4ROOTVECGEOMINCNAVIGATOR_H_
#define G4VECGEOM_INCLUDE_TG4ROOTVECGEOMINCNAVIGATOR_H_

#include "G4Navigator.hh"
#include "TG4RootNavigator.h"
#include <VecGeom/navigation/NavigationState.h>
#include <VecGeom/base/Vector3D.h>
#include "G4ThreeVector.hh"
#include "FastG4VecGeomLookup.h"

class TG4VecGeomDetectorConstruction;
struct FastG4VecGeomLookup;

/// \brief A specialization of the TG4RootNavigator using directly a VecGeom geometry but only in certain parts.
///
/// Navigator specializing the TG4RootNavigator. This is for users of TGeo who
/// still want to use VecGeom at simulation runtime.
///
/// Only few incremental interfaces are specialized to start with.
///
/// \author S. Wenzel; CERN

#define REPLACE_COMPUTESAFETY 1

class TG4RootVecGeomIncNavigator : public TG4RootNavigator {

protected:
  FastG4VecGeomLookup fG4VGLookup; ///< G4VecGeom detector construction
                                   ///< which is the structure keeping geometry lookups etc.
  vecgeom::NavigationState *fCurState  = nullptr;
  vecgeom::NavigationState *fTestState = nullptr;

private:
  void PrintState(); // print info about navigatio state (exiting, ...)

  const vecgeom::Vector3D<double> GetLocalVGPoint(G4ThreeVector const &) const;

public:
  TG4RootVecGeomIncNavigator(TG4RootDetectorConstruction const *);

  ~TG4RootVecGeomIncNavigator() override = default;

#ifdef REPLACE_COMPUTESAFETY
  G4double ComputeSafety(const G4ThreeVector &globalpoint, const G4double pProposedMaxLength,
                         const G4bool keepState) override;
#endif
};

inline const vecgeom::Vector3D<double> TG4RootVecGeomIncNavigator::GetLocalVGPoint(G4ThreeVector const &p) const
{
  const auto localpoint = fHistory.GetTopTransform().TransformPoint(p);
  return vecgeom::Vector3D<double>(localpoint[0], localpoint[1], localpoint[2]);
}

#endif /* G4VECGEOM_INCLUDE_TG4ROOTVECGEOMINCNAVIGATOR_H_ */
