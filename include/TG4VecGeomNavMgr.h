/*
 * TG4VecGeomNavMgr.h
 *
 *  Created on: Mar 1, 2018
 *      Author: swenzel
 */

#ifndef G4VECGEOM_INCLUDE_TG4VECGEOMNAVMGR_H_
#define G4VECGEOM_INCLUDE_TG4VECGEOMNAVMGR_H_

/// \file TG4VecGeomNavMgr.h
/// \brief Definition of the TG4VecGeomNavMgr class
///
/// \author S. Wenzel; CERN

#include "G4Threading.hh"

class TG4VecGeomNavigator;
class TG4VecGeomDetectorConstruction;
class TVirtualUserPostDetConstruction;

// from VecGeom
#include <VecGeom/management/GeoManager.h>

/// \brief Manager class creating a G4Navigator using a VecGeom geometry.
///
/// \author S. Wenzel ; CERN

using GeoManager_t = vecgeom::GeoManager;

class TG4VecGeomNavMgr {

protected:
  GeoManager_t const *fVecGeomGeoMgr                       = nullptr; ///< Pointer to VecGeom geometry manager
  TG4VecGeomNavigator *fNavigator                          = nullptr; ///< pointer to G4 navigator working with VecGeom
  TG4VecGeomDetectorConstruction *fDetConstruction         = nullptr; ///< G4 geometry built based on ROOT one
  TVirtualUserPostDetConstruction *fPostDetDetConstruction = nullptr; ///< User defined initialization
  bool fConnected;                                                    ///< flagging connection status to G4

  TG4VecGeomNavMgr() = default;

private:
  static G4ThreadLocal TG4VecGeomNavMgr *fVecGeomNavMgr; ///< Static pointer to singleton
  static TG4VecGeomNavMgr *fgMasterInstance;

public:
  static TG4VecGeomNavMgr *GetInstance(GeoManager_t const &geom);
  static TG4VecGeomNavMgr *GetInstance(TG4VecGeomNavMgr const &);
  static TG4VecGeomNavMgr *GetMasterInstance();
  virtual ~TG4VecGeomNavMgr();

  TG4VecGeomNavMgr(GeoManager_t const &geom, TG4VecGeomDetectorConstruction *detConstruction = nullptr);

  bool ConnectToG4();
  void Initialize(TVirtualUserPostDetConstruction *sdinit = nullptr, int nthreads = 1);

  // Test utilities
  void PrintG4State() const;
  void SetVerboseLevel(int level);

  void SetNavigator(TG4VecGeomNavigator *nav);
  TG4VecGeomNavigator *GetNavigator() const { return fNavigator; }

  /// Return the G4 geometry built based on a VecGeom one
  TG4VecGeomDetectorConstruction *GetDetConstruction() const { return fDetConstruction; }
};

#endif /* G4VECGEOM_INCLUDE_TG4VECGEOMNAVMGR_H_ */
