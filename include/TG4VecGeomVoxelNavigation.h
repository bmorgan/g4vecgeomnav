/*
 * TG4VecGeomVoxelNavigation.h
 *
 *  Created on: Mar 1, 2018
 *      Author: swenzel
 */

#ifndef G4VECGEOM_INCLUDE_TG4VECGEOMVOXELNAVIGATION_H_
#define G4VECGEOM_INCLUDE_TG4VECGEOMVOXELNAVIGATION_H_

#include "G4VoxelNavigation.hh"

struct FastG4VecGeomLookup;

/// \brief A specialization of the G4VoxelNavigation
/// using a VecGeom geometry and VecGeom acceleration structures
///
/// \author S. Wenzel; CERN
class TG4VecGeomVoxelNavigation : public G4VoxelNavigation {

protected:
  const FastG4VecGeomLookup *fG4VGLookup; ///< G4VecGeom detector construction
                                          ///< which is the structure keeping geometry lookups etc.

private:
public:
  TG4VecGeomVoxelNavigation() = default;
  TG4VecGeomVoxelNavigation(FastG4VecGeomLookup const &dc);
  ~TG4VecGeomVoxelNavigation() override = default;

  // overriding virtual method to compute local step (given local coordinates)
  // is used/called from G4Navigator
  double ComputeStep(const G4ThreeVector &localPoint, const G4ThreeVector &localDirection,
                     const G4double currentProposedStepLength, G4double &newSafety, G4NavigationHistory &history,
                     G4bool &validExitNormal, G4ThreeVector &exitNormal, G4bool &exiting, G4bool &entering,
                     G4VPhysicalVolume *(*pBlockedPhysical), G4int &blockedReplicaNo) override;

  // overriding virtual method to locate containing volume
  // is used/called from G4Navigator
  G4bool LevelLocate(G4NavigationHistory &history, const G4VPhysicalVolume *blockedVol, const G4int blockedNum,
                     const G4ThreeVector &globalPoint, const G4ThreeVector *globalDirection,
                     const G4bool pLocatedOnEdge, G4ThreeVector &localPoint) override;

  void SetG4ComparisonMode(bool val) { fG4ComparisonMode = val; }
  //  Compare results of VecGeom navigation with reference Geant4 results
  void SetUseVGresults(bool val) { fUseVGresults = val; }
  //  If comparing, use VecGeom results during navigation (after comparison) - else use Geant4 reference values
  void SetVerbose(bool val) { fVerbose = val; }
  //  Set Verbosity - if true, each call will report information

private:
  bool fG4ComparisonMode = false; // Compare results 'in flight' - VecGeom results vs G4 navigator results
  // In comparison mode, which results are used to navigate is governed by the flag below:
  bool fUseVGresults = true; // Use VecGeom navigation results -- even in g4Comparison mode.

  bool fVerbose = false;
};

#endif /* G4VECGEOM_INCLUDE_TG4VECGEOMVOXELNAVIGATION_H_ */
