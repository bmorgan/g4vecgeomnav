/*
 * FastG4VecGeomLookup.h
 *
 *  Created on: Mar 19, 2018
 *      Author: swenzel
 */

#ifndef G4VECGEOM_INCLUDE_FASTG4VECGEOMLOOKUP_H_
#define G4VECGEOM_INCLUDE_FASTG4VECGEOMLOOKUP_H_

#include <vector>
#include <VecGeom/volumes/PlacedVolume.h>
#include "G4VPhysicalVolume.hh"

using PVolume_t = vecgeom::VPlacedVolume;
class G4VPhysicalVolume;

#define ACTIVATEREPLICATION

// class to gain access to unique ID for physical volume
class MYG4VP : public G4VPhysicalVolume {
public:
  int getID() const { return instanceID; }
};

#ifndef ACTIVATEREPLICATION
// this is ok for versions without replicas
struct FastG4VecGeomLookup {
  // a fast lookup using the fact that VecGeom placed volumes have
  // a contiguous linear index
  std::vector<G4VPhysicalVolume *> fPV_VGtoG4_Vector;
  // the inverse is also true
  std::vector<const PVolume_t *> fPV_G4toVG_Vector;

  // interfaces
  G4VPhysicalVolume *VGToG4(int vgid, int &replica, bool &isreplica) const
  {
    replica   = 0; // replica not used in this case
    isreplica = false;
    return fPV_VGtoG4_Vector[vgid];
  }
  PVolume_t const *G4ToVG(int g4id, int g4replica = 0) const
  {
    (void)g4replica; // replica not used in this case
    assert(g4replica == 0);
    return fPV_G4toVG_Vector[g4id];
  }

  void Insert(std::vector<PVolume_t const *> const *vgnodes, G4VPhysicalVolume const *g4node)
  {
    assert(vgnodes->size() == 1);
    auto singlenode                                = (*vgnodes)[0];
    fPV_VGtoG4_Vector[singlenode->id()]            = const_cast<G4VPhysicalVolume *>(g4node);
    fPV_G4toVG_Vector[((MYG4VP *)g4node)->getID()] = singlenode;
  }
  void Insert(PVolume_t const *vgnode, G4VPhysicalVolume const *g4node)
  {
    fPV_VGtoG4_Vector[vgnode->id()]                = const_cast<G4VPhysicalVolume *>(g4node);
    fPV_G4toVG_Vector[((MYG4VP *)g4node)->getID()] = vgnode;
  }

  void InitSize(size_t vgnodes)
  {
    fPV_VGtoG4_Vector.resize(vgnodes);
    fPV_G4toVG_Vector.resize(vgnodes);
  }
};

#else

// this is appropriate for versions with replicas
struct FastG4VecGeomLookup {
  std::vector<std::pair<G4VPhysicalVolume *, int>> fPV_VGtoG4_Vector;

  // mapping of g4id + g4replica to a vecgeom placed volume
  // for the moment we take a vector<vector> which should be improved towards
  // a more cache friendly and smaller memory usage solution
  std::vector<std::vector<const PVolume_t *> const *> fPV_G4toVG_Vector;

  // interfaces
  G4VPhysicalVolume *VGToG4(int vgid, int &replica, bool &isreplica) const
  {
    const auto p = fPV_VGtoG4_Vector[vgid];
    replica      = p.second;
    // unset sign bit
    replica &= ~(1 << 31);
    isreplica = p.second & 1 << 31; // sign bit encodes replica property
    return p.first;
  }

  PVolume_t const *G4ToVG(int g4id, int g4replica = 0) const
  {
    assert(g4replica < fPV_G4toVG_Vector[g4id]->size());
    return (*fPV_G4toVG_Vector[g4id])[g4replica];
  }

  void InitSize(size_t vgnodes)
  {
    fPV_VGtoG4_Vector.resize(vgnodes, std::pair<G4VPhysicalVolume *, int>(nullptr, 0));
    fPV_G4toVG_Vector.resize(vgnodes);
  }

  void Insert(std::vector<PVolume_t const *> const *vgnodes, G4VPhysicalVolume const *g4node)
  {
    assert(vgnodes->size() >= 1);
    int replica = 0;
    for (auto singlenode : *vgnodes) {
      int code = replica;
      if (vgnodes->size() > 1) {
        code |= 1 << 31;
      }
      // std::cerr << "INSERTING CODE " << code << "for node " << singlenode->GetLabel() << "\n";
      fPV_VGtoG4_Vector[singlenode->id()] =
          std::pair<G4VPhysicalVolume *, int>(const_cast<G4VPhysicalVolume *>(g4node), code);
      replica++;
    }
    fPV_G4toVG_Vector[((MYG4VP *)g4node)->getID()] = vgnodes;
  }

  void Insert(PVolume_t const *singlenode, G4VPhysicalVolume const *g4node)
  {
    auto vgnodes = new std::vector<PVolume_t const *>;
    vgnodes->emplace_back(singlenode);
    Insert(vgnodes, g4node);
  }
};
#endif

#endif /* G4VECGEOM_INCLUDE_FASTG4VECGEOMLOOKUP_H_ */
